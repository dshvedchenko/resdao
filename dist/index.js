'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = doaLib;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _gridfsStream = require('gridfs-stream');

var _gridfsStream2 = _interopRequireDefault(_gridfsStream);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

var _refreshToken = require('./refreshToken');

var _refreshToken2 = _interopRequireDefault(_refreshToken);

var _file = require('./file');

var _file2 = _interopRequireDefault(_file);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function doaLib(config) {
    _mongoose2.default.Promise = global.Promise;
    _mongoose2.default.connect(config.mongo);

    const conn = _mongoose2.default.connection;

    _gridfsStream2.default.mongo = _mongoose2.default.mongo;
    const gfs = (0, _gridfsStream2.default)(conn.db);

    _mongoose2.default.set('debug', config.mongoDebug);
    const User = _mongoose2.default.model('User', _user2.default);
    const File = _mongoose2.default.model('File', _file2.default);
    const RefreshToken = _mongoose2.default.model('RefreshToken', _refreshToken2.default);

    return {
        User,
        RefreshToken,
        File,
        conn,
        gfs
    };
}
//# sourceMappingURL=index.js.map