'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _refreshToken = require('./refreshToken');

var _refreshToken2 = _interopRequireDefault(_refreshToken);

var _debugger = require('../lib/debugger');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const makePwdUpdatedAt = () => Math.floor(+new Date() / 1000);

function beforeUpdate(next) {
    // log('HOOK: ', this)
    if (this.password !== undefined || this._update !== undefined && this._update.$set !== undefined && this._update.$set.password !== undefined) {
        (0, _debugger.database)(makePwdUpdatedAt());
        this.update({}, { $set: { passwordLastSet: makePwdUpdatedAt() } });
    }
    this.update({}, { $set: { updatedAt: Date.now() } });
    next();
}

const schema = new _mongoose2.default.Schema({
    username: { type: String, index: true, required: true, unique: true },
    password: String,
    email: { type: String, index: true, required: true, unique: true },
    enabled: { type: Boolean, default: true },
    admin: { type: Boolean, default: false },
    passwordLastSet: { type: Number, default: makePwdUpdatedAt() },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
    refreshTokens: [_refreshToken2.default]
});

schema.pre('update', beforeUpdate);
schema.pre('save', beforeUpdate);

exports.default = schema;
//# sourceMappingURL=user.js.map