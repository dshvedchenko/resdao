'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = new _mongoose2.default.Schema({
    token: String,
    createdAt: { type: Date, default: Date.now() },
    expiration: Number
});
//# sourceMappingURL=refreshToken.js.map