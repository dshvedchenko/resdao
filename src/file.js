import mongoose from 'mongoose';

export default new mongoose.Schema({
    fileName: String,
    fileId: String
})