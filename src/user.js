import mongoose from 'mongoose';
import debug from 'debug';

import RefreshTokenSchema from './refreshToken';

export const log = debug('app:database');

const makePwdUpdatedAt = () => Math.floor((+new Date() / 1000));

function beforeUpdate(next) {
    // log('HOOK: ', this)
    if (this.password !== undefined ||
        this._update !== undefined &&
        this._update.$set !== undefined &&
        this._update.$set.password !== undefined) {
        log(makePwdUpdatedAt());
        this.update({}, { $set: { passwordLastSet: makePwdUpdatedAt() } });
    }
    this.update({}, { $set: { updatedAt: Date.now() } });
    next();
}

const schema = new mongoose.Schema({
    username: { type: String, index: true, required: true, unique: true },
    password: String,
    email: { type: String, index: true, required: true, unique: true },
    enabled: { type: Boolean, default: true },
    admin: { type: Boolean, default: false },
    passwordLastSet: { type: Number, default: makePwdUpdatedAt() },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
    refreshTokens: [RefreshTokenSchema]
})

schema.pre('update', beforeUpdate);
schema.pre('save', beforeUpdate);

export default schema;