import mongoose from 'mongoose';

export default new mongoose.Schema({
    token: String,
    createdAt: { type: Date, default: Date.now() },
    expiration: Number
})