import mongoose from 'mongoose';
import Grid from 'gridfs-stream';
import UserSchema from './user';
import RefreshTokenSchema from './refreshToken';
import FileSchema from './file';

export default function doaLib(config) {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.mongo);

    const conn = mongoose.connection;

    Grid.mongo = mongoose.mongo;
    const gfs = Grid(conn.db);

    mongoose.set('debug', config.mongoDebug);
    const User = mongoose.model('User', UserSchema);
    const File = mongoose.model('File', FileSchema);
    const RefreshToken = mongoose.model('RefreshToken', RefreshTokenSchema);

    return {
        User,
        RefreshToken,
        File,
        conn,
        gfs
    }
}